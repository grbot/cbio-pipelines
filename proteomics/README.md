
Test suite

Access the proteogenomics test suite and data from public repository available here::
https://thys_potgieter@bitbucket.org/thys_potgieter/cbio-proteogenomics-tests.git


This will be moved to the script for peptideshaker pipelines:
# To convert Q exactive Thermo RAW files to mgf
#$ msconvert.exe * --64 --zlib --filter "peakPicking true 1-" --mgf


