# README #

All code has been moved to GitHub. This BitBucket repos has been changed to read only mode.

* `cbio-pipelines/16S and cbio-pipelines/qc` to https://github.com/uct-cbio/16S-pipeline
* `cbio-pipelines/proteomics` to https://github.com/uct-cbio/cbio_proteomics
* `cbio-pipelines/Strep_WGS_pbp_pipeline` to https://github.com/uct-cbio/Strep_WGS_pbp_pipeline

